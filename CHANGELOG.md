# Changelog

All notable changes to `ch-pagebuilder` will be documented in this file

## 1.0.0 - 201X-XX-XX

- initial release

## 1.0.4

- options to drag sections
- options to drag fields 
- added button field
- restricted access
